﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JornDemo.Data
{
    public class CurrencyDB : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=Currencies.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyValue>()
                .HasKey(c => new { c.Name, c.Date });
        }

        public DbSet<CurrencyValue> CurrencyValues { get; set; }
    }
}
