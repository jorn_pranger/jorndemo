﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace JornDemo.Data
{
    public class CurrencyValue
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime InsertDate { get; set; }
        public double Value { get; set; }
        /// </summary>
        public string Source { get; set; }
    }
}
