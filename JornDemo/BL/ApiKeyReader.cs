﻿using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace JornDemo.BL
{
    public interface IApiKeyReader
    {
        bool HasApiKey { get; }

        string ApiKey { get; }

    }
    internal class ApiKeyReader: IApiKeyReader
    {
        public bool HasApiKey { get; private set; }

        public string ApiKey { get; private set; }
        
        public ApiKeyReader(IHostingEnvironment env)
        {
            string currencyKeyFilePath = $"{env.ContentRootPath}\\api.key";
            Initialize(currencyKeyFilePath);
        }

        private void Initialize(string currencyKeyFilePath)
        {
            try
            {
                if (File.Exists(currencyKeyFilePath))
                    ApiKey = File.ReadAllText(currencyKeyFilePath);
                HasApiKey = !string.IsNullOrEmpty(ApiKey);
            }
            catch
            {
                //INTENTIONALLY LEFT EMPTY
            }
        }
    }
}
