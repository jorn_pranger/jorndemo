﻿using JornDemo.BL.CurrencyGetters.Base;
using JornDemo.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JornDemo.BL.CurrencyGetters
{

    public interface ICurrencyLogic
    {
        void AddValueFromCurrencyGetter(string currencyName, DateTime date);

        List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName);


        List<CurrencyValue> GetMonthlyCurrencyValuesFromDataStore(string currencyName, DateTime monthEnd);


        List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName, DateTime lastUpdateDate);

        List<string> GetSuportedCurrencyCodes();
        List<string> GetStoredCurrencies();
    }

    internal class CurrencyLogic : ICurrencyLogic
    {
        private CurrencyDB CurrencyDB { get; set; }

        private ILogger<CurrencyLogic> Logger { get; set; }

        private ICurrencyGetter CurrencyGetter { get; set; }

        public CurrencyLogic(CurrencyDB db, ILogger<CurrencyLogic> logger, ICurrencyGetter getter)
        {
            CurrencyDB = db;
            Logger = logger;
            CurrencyGetter = getter;
        }

        public void AddValueFromCurrencyGetter(string currencyName, DateTime date)
        {
            double currencyValue = CurrencyGetter.GetCurrencyValue(currencyName, date);
            Logger.LogInformation($"Add new currency value for {currencyName}  {date}");
            CurrencyValue val = new CurrencyValue();
            CurrencyDB.Add(new CurrencyValue() { Date = date, Name = currencyName.ToUpper(), Value = currencyValue, InsertDate=DateTime.Now, Source = CurrencyGetter.GetType().ToString() });
            CurrencyDB.SaveChanges();
        }

        public List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName)
        {
            Logger.LogInformation($"GetCurrencyValuesFromDataStore for {currencyName}");

            var currencyValues = (from currencies in CurrencyDB.CurrencyValues
                                  where currencyName.Equals(currencies.Name, StringComparison.InvariantCultureIgnoreCase)
                                  orderby currencies.Date descending
                                  select currencies).ToList();
            return currencyValues;
        }


        public List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName, DateTime lastUpdateDate)
        {
            Logger.LogInformation($"GetCurrencyValuesFromDataStore for {currencyName}");

            var currencyValues = (from currencies in CurrencyDB.CurrencyValues
                                  where currencyName.Equals(currencyName, StringComparison.InvariantCultureIgnoreCase)
                                  && lastUpdateDate < currencies.InsertDate
                                  orderby currencies.Date descending
                                  select currencies).ToList();
            return currencyValues;
        }

        public List<string> GetSuportedCurrencyCodes()
        {
            return CurrencyGetter.GetSuportedCurrencyCodes();
        }

        public List<string> GetStoredCurrencies()
        {
            var currencySources = (from currencies in CurrencyDB.CurrencyValues
                                  select currencies.Name).Distinct().ToList();
            return currencySources;
        }

        public List<CurrencyValue> GetMonthlyCurrencyValuesFromDataStore(string currencyName, DateTime monthEnd)
        {
            var currencyValues = (from currencies in CurrencyDB.CurrencyValues
                                  where currencyName.Equals(currencies.Name, StringComparison.InvariantCultureIgnoreCase)
                                  && monthEnd.AddMonths(-1) < currencies.Date 
                                  && monthEnd >= currencies.Date
                                  orderby currencies.Date descending                
                                  select currencies).ToList();
            return currencyValues;
        }
    }
}
