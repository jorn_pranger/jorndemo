﻿using JornDemo.BL.CurrencyGetters.Base;
using System;
using System.Collections.Generic;

namespace JornDemo.BL.CurrencyGetters
{
    internal class RandomCurrencyGetter : CurrencyGetterBase, ICurrencyGetter
    {
        public double GetCurrencyValue(string currencyName, DateTime date)
        {
            UpdateLatestUpdatesList(currencyName);
            return GetRandomNumber(1, 20);
        }

        public List<string> GetSuportedCurrencyCodes()
        {
            return new List<string>() { "BTC", "USD", "EUR" };
        }

        private double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}
