﻿using JornDemo.BL.CurrencyGetters.Base;
using JornDemo.BL.CurrencyGetters.Helper;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace JornDemo.BL.CurrencyGetters
{
    public class ApiCurrencyGetter : CurrencyGetterBase, ICurrencyGetter
    {
        private IApiKeyReader Reader { get; set; }

        private IApiResponseHelper ApiResponseHelper { get; set; }

        private List<string> _suportedCurrencies { get; set; }

        public ApiCurrencyGetter(IApiKeyReader reader, IApiResponseHelper responseHelper)
        {
            Reader = reader;
            ApiResponseHelper = responseHelper;
        }

        public double GetCurrencyValue(string currencyName, DateTime date)
        {
            string requestUrl = $"http://apilayer.net/api/historical?access_key={Reader.ApiKey}&currencies={currencyName}&date={date.ToString("yyyy-MM-dd")}";
            string jsonResult = ApiResponseHelper.GetApiResponse(requestUrl).Result;
            JObject jObject = JObject.Parse(jsonResult);
            //{"success":true,"terms":"https:\/\/currencylayer.com\/terms","privacy":"https:\/\/currencylayer.com\/privacy","historical":true,"date":"2017-01-01","timestamp":1483315199,"source":"USD","quotes":{"USDBTC":0.001002}}
            var usdBTC = jObject["quotes"][$"USD{currencyName}"].Value<double>();
            UpdateLatestUpdatesList(currencyName);

            //we want the BTC value in USD not the USD in BTC
            return 1 / usdBTC;
        }

        public List<string> GetSuportedCurrencyCodes()
        {
            if (_suportedCurrencies == null)
            {
                _suportedCurrencies = new List<string>();
                string requestUrl = $"http://apilayer.net/api/list?access_key={Reader.ApiKey}";
                string jsonResult = ApiResponseHelper.GetApiResponse(requestUrl).Result;
                JObject jObject = JObject.Parse(jsonResult);
                foreach (var token in jObject["currencies"])
                {
                    JProperty jProperty = token.ToObject<JProperty>();
                    _suportedCurrencies.Add(jProperty.Name);
                }
            }
            return _suportedCurrencies;
        }
    }
}