﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JornDemo.BL.CurrencyGetters.Base
{
    public class CurrencyGetterBase
    {
        public List<KeyValuePair<string, DateTime>> LastestUpdates { get; private set; } = new List<KeyValuePair<string, DateTime>>();

        public void UpdateLatestUpdatesList(string currencyName)
        {
            LastestUpdates.Add(new KeyValuePair<string, DateTime>(currencyName, DateTime.Now));
        }
    }
}
