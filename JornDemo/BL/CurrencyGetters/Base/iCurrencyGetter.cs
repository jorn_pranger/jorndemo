﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JornDemo.BL.CurrencyGetters.Base
{
    public interface ICurrencyGetter
    {
        List<KeyValuePair<string, DateTime>> LastestUpdates { get; }

        List<string> GetSuportedCurrencyCodes();

        double GetCurrencyValue(string currencyName, DateTime date);
    }
}
