﻿using System.Net.Http;
using System.Threading.Tasks;

namespace JornDemo.BL.CurrencyGetters.Helper
{
    public class ApiResponseHelper : IApiResponseHelper
    {
        public async Task<string> GetApiResponse(string requestUrl)
        {
            using (var client = new System.Net.Http.HttpClient())
            {
                var response = await client.GetAsync(requestUrl, HttpCompletionOption.ResponseHeadersRead);
                response.EnsureSuccessStatusCode();
                var data = await response.Content.ReadAsStringAsync();
                return data;
            }
        }
    }

    public interface IApiResponseHelper
    {
        Task<string> GetApiResponse(string requestUrl);
    }
}
