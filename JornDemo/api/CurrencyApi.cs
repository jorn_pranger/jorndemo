﻿using JornDemo.BL;
using JornDemo.BL.CurrencyGetters;
using JornDemo.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;


namespace JornDemo.api
{

    [ApiController]
    public class CurrencyApi : ControllerBase
    {
        private ICurrencyLogic CurrencyLogic { get; set; }
        public CurrencyApi(ICurrencyLogic currencyLogic)
        {
            CurrencyLogic = currencyLogic;
        }

        [HttpGet]
        [Route("/api/CurrencyCodes")]
        public List<string> GetCurrencyCodes()
        {
            return CurrencyLogic.GetSuportedCurrencyCodes();
        }

        [HttpGet]
        [Route("/api/GetStoredCurrencies")]
        public List<string> GetStoredCurrencies()
        {
            return CurrencyLogic.GetStoredCurrencies();
        }

        [HttpGet]
        [Route("/api/GetCurrency/{currencyName}")]
        public List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName)
        {
            return CurrencyLogic.GetCurrencyValuesFromDataStore(currencyName);
        }

        [HttpGet]
        [Route("/api/GetMonthlyCurrency/{currencyName}/{monthEndDate}")]
        public List<CurrencyValue> GetMontlyCurrencyValuesFromDataStore(string currencyName, DateTime monthEndDate)
        {
            return CurrencyLogic.GetMonthlyCurrencyValuesFromDataStore(currencyName, monthEndDate);
        }


        [HttpGet]
        [Route("/api/GetCurrency/{currencyName}/{previousUpdate}")]
        public List<CurrencyValue> GetCurrencyValuesFromDataStore(string currencyName, DateTime previousUpdate)
        {
            return CurrencyLogic.GetCurrencyValuesFromDataStore(currencyName, previousUpdate);
        }

        [HttpPost]
        [Route("/api/AddCurrency/{currencyName}")]
        public void AddCurrency(string currencyName, DateTime currencyDate)
        {
            CurrencyLogic.AddValueFromCurrencyGetter(currencyName, currencyDate);
        }
    }
}