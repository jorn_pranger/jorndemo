﻿using JornDemo.BL;
using JornDemo.BL.CurrencyGetters;
using JornDemo.BL.CurrencyGetters.Base;
using JornDemo.BL.CurrencyGetters.Helper;
using JornDemo.Data;
using JornDemo.Signalr;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace JornDemo
{
    public class Startup
    {
        private readonly ILogger _logger;

        private readonly IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {

            _env = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("_myAllowSpecificOrigins",
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200")
                      .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials();
                });
            });

            services.AddEntityFrameworkSqlite().AddDbContext<CurrencyDB>();
            services.AddSingleton<IApiKeyReader, ApiKeyReader>();
            services.AddMvc();
            services.AddSignalR();
            if (new ApiKeyReader(_env).HasApiKey)
            {
                services.AddTransient<IApiResponseHelper, ApiResponseHelper>();
                //this should be singleton as free license only allows x nr of requests
                //maintains state
                services.AddSingleton<ICurrencyGetter, ApiCurrencyGetter>();
            }
            else
            {
                services.AddTransient<ICurrencyGetter, RandomCurrencyGetter>();
            }
            services.AddTransient<ICurrencyLogic, CurrencyLogic>();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("_myAllowSpecificOrigins");
            using (var client = new CurrencyDB())
            {
                client.Database.EnsureCreated();
            }
            app.UseStaticFiles();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=BasePage}/{action=Index}");
            });

            app.UseSignalR(routes =>
            {
                routes.MapHub<CurrencyHub>("/CurrencyHub");
            });


        }
    }
}
