﻿using JornDemo.BL.CurrencyGetters;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JornDemo.Signalr
{
    public class CurrencyHub : Hub
    {
        private ICurrencyLogic CurrencyLogic { get; set; }
        public CurrencyHub(ICurrencyLogic currencyLogic)
        {
            CurrencyLogic = currencyLogic;
        }

        public  async Task AddCurrency(string currencyName, DateTime currencyDate)
        {
            CurrencyLogic.AddValueFromCurrencyGetter(currencyName, currencyDate);
            await Clients.All.SendAsync("UpdatedCurrency", currencyName, currencyDate); 
        }

    }
}
