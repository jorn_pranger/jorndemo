﻿(function ($) {
    $.fn.CurrencyElem= function (options) {
        var settings = $.extend({
            // These are the defaults.
            valutaSelector: "[data-role='valuta']",
            dateSelector: "[data-role='date']",
            buttonSelector: "[data-role='button']",
            chartContainerSelector: "#chartContainer"
          

        }, options);
        let baseElement = this;
        let connection = new signalR.HubConnectionBuilder()
            .withUrl("/CurrencyHub")
            .configureLogging(signalR.LogLevel.Information)
            .build();


        $(settings.buttonSelector).click(function (e) {
            
            connection.invoke("AddCurrency", $(settings.valutaSelector).val(), $(settings.dateSelector).val()).catch(err => console.error(err.toString()));
            e.preventDefault();
        });

        $.getJSON("/api/CurrencyCodes", function (result) {
            var options = $(settings.valutaSelector);
            $.each(result, function () {
                options.append($("<option />").val(this).text(this));
            });
        });

        connection.on("UpdatedCurrency", (currencyName, time) => {
            if ($('#chart' + currencyName).length > 0) {
                //replace chart
                $('#chart' + currencyName).ValutaChart({ valuta: currencyName });
            }
            else {
                let topElem = $("<div class='column is-4'><h3 class='title is-3'>" + currencyName + "</h3></div>");
                let newElem = $("<div class='box ct-chart ct-perfect-fourth'></div>");
                newElem.attr("id", "chart" + currencyName);
                $(topElem).append(newElem);
                $(settings.chartContainerSelector).append(topElem);
                newElem.ValutaChart({ valuta: currencyName });
            }
        });

        async function start() {
            try {
                await connection.start();
                console.log("connected");
            } catch (err) {
                console.log(err);
                setTimeout(() => start(), 5000);
            }
        };
        start();

        return this;
    };
}(jQuery));
