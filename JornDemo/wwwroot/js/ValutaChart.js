﻿(function ($) {

    $.fn.ValutaChart = function (options) {

        // Establish our default settings
        var settings = $.extend({
            valuta: 'BTC'
        }, options);

        var element = this;
        if (typeof (Chartist) != "undefined") {
            let Labels = [];
            let Series = [[]];

            $.getJSON("/api/GetCurrency/" + settings.valuta, function (result) {
                $.each(result, function () {
                    Labels.push(new Date(this.date));
                    Series[0].push(this.value);
                });

                let data = {
                    labels: Labels,
                    series: Series
                };

                let chartOptions = {
                    seriesBarDistance: 15,
                    axisX: {
                        offset: 60,
                        labelInterpolationFnc: function (d) {
                            let curr_date = d.getDate();
                            let curr_month = d.getMonth() + 1; //Months are zero based
                            let curr_year = d.getFullYear();
                            return(curr_date + "-" + curr_month + "-" + curr_year);
                        }
                    }
                };

                // All you need to do is pass your configuration as third parameter to the chart function
                new Chartist.Bar('#' + element[0].id , data, chartOptions);
            });
        
          
        }
    }

}(jQuery));