﻿(function ($) {$.fn.InitCharts = function () {
        let element = this;
        $.getJSON("/api/GetStoredCurrencies", function (result) {
        
            $.each(result, function () {
                let topElem = $("<div class='column is-4'><h3 class='title is-3'>" + this + "</h3></div>");
                let newElem = $("<div class='box ct-chart ct-perfect-fourth'></div>");
                newElem.attr("id", "chart" + this);
                $(topElem).append(newElem);
                $(element).append(topElem);
                newElem.ValutaChart({ valuta: this });
            });
        });
    }
}(jQuery));