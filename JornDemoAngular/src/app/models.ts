
    export class CurrencyValue
    {
        name: string;
        date: Date;
        insertDate: Date;
        value: number;
        source: string;
    }

    export class CurrencySelection
    {
        name: string;
        date: Date; 
    }