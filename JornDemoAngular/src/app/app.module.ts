import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DemoApiService } from './services/demoapi.service'
import { SignalRService } from './services/signal-r.service'
import {  HttpClientModule } from '@angular/common/http';
import { CurrencyDropdownComponent } from './currency-dropdown/currency-dropdown.component'
import 'bulma/css/bulma.css';
import { InputSectionComponent } from './input-section/input-section.component';
import { DateComponent } from './date/date.component'
import { FormsModule } from '@angular/forms';
import { CurrencyChartComponent } from './currency-chart/currency-chart.component';
import { ChartistModule } from 'ng-chartist';
import { OverviewComponent } from './overview/overview.component';
import { DetailsComponent } from './details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    CurrencyDropdownComponent,
    InputSectionComponent,
    DateComponent,
    CurrencyChartComponent,
    OverviewComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ChartistModule
  ],
  providers: [    DemoApiService, SignalRService],
  bootstrap: [AppComponent]
})
export class AppModule { }
