import { Component, OnInit } from '@angular/core';
import { DemoApiService } from '../services/demoapi.service';
import { SignalRService } from '../services/signal-r.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.sass']
})
export class OverviewComponent implements OnInit {

 
  title: string = 'JornDemoAngular';
  service: DemoApiService;
  signalR: SignalRService;
currencyNames: string[];

  constructor(service: DemoApiService, signalR: SignalRService )  
  { 
    this.service = service;
    this.signalR = signalR;
  }

  ngOnInit()
  {
    this.signalR.startConnection();
    this.signalR.addTransferChartDataListener();
    this.service.getStoredCurrencies().subscribe(data =>{
      this.currencyNames = data;
    });  
    this.signalR.receivedUpdate.subscribe((data: string) => {
      if (!(this.currencyNames.indexOf(data) >= 0)) {
        this.updateChart();
      }
    });
  }
 
  updateChart()
  {
    this.service.getStoredCurrencies().subscribe(data =>{
      this.currencyNames = data;
    });  
  }

}
