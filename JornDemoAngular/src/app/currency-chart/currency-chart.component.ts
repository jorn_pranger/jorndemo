import { Component, OnInit, Input } from '@angular/core';
import { CurrencyValue } from '../models';
import {  ChartType } from 'ng-chartist';
import { IBarChartOptions, ILineChartOptions } from 'chartist';
import { DemoApiService } from '../services/demoapi.service';
import { SignalRService } from '../services/signal-r.service';

@Component({
  selector: 'app-currency-chart',
  templateUrl: './currency-chart.component.html',
  styleUrls: ['./currency-chart.component.sass']
})

export class CurrencyChartComponent implements OnInit {
  
  private _name: string;

  @Input() type: ChartType = 'Bar';
  @Input() name: string;
  @Input() showAll: boolean;
  currencies: CurrencyValue[];
  data:  IChartData;
  options: any;
  service: DemoApiService;
  signalR: SignalRService;
  lastUpdate:Date;
  constructor(service: DemoApiService, signalR: SignalRService) {
    this.service = service;
    this.signalR = signalR;
  }

  ngOnInit() {
    this.setChartOptions(); 
    this.getCurrencies();    
    this.signalR.receivedUpdate.subscribe((data: string) => {
      if (data.toUpperCase() === this.name.toUpperCase()) {
        this.updateCurrencies();
      }
    });
  }

  private setChartOptions()
  {
    switch (this.type) {
      case "Bar":
        this.setBarChartOptions();
        break;
      case "Line":
        this.setLineChartOptions();
        break;
    }
  }

  private setBarChartOptions() {
    this.options = <IBarChartOptions>{
      height: '200px', axisX: {
        labelInterpolationFnc: function (value, index, chart) {
          let lenght: number = 0;

          if (chart) lenght = chart.length;
          if (lenght > 10)
            return index % 10 === 0 ? value : null;
          else
            return value;
        }
      }
    };
  }

  private setLineChartOptions() {
    this.options = <ILineChartOptions>{
      height: '800px', axisX: {
        labelInterpolationFnc: function (value, index, chart) {
          let lenght: number = 0;

          if (chart) lenght = chart.length;
          if (lenght > 10)
            return index % 100 === 0 ? value : null;
          else
            return value;
        }
      }, axisY: {
        labelInterpolationFnc: function (value, index, chart) {
          let lenght: number = 0;

          if (chart) lenght = chart.length;
          if (lenght > 10)
            return index % 10 === 0 ? value : null;
          else
            return value;
        }
      }
    }
  }
  getCurrencies() {
    this.lastUpdate = new Date();
    if (this.showAll) {
      this.service.getCurrency(this.name).subscribe(data => {
        this.currencies = data;
        this.getLabelsAndValues();
      });
    }
    else {
      this.service.getMonthCurrency(this.name, new Date()).subscribe(data => {
        this.currencies = data;
        this.getLabelsAndValues();
      });
    }
  }

  updateCurrencies() {
     {
      this.service.getUpdatedCurrency(this.name,this.lastUpdate).subscribe(data => {
        var self = this;
        data.forEach(element => {
          if(self.currencies.indexOf(element) === -1)
          {
            self.currencies.push(element);
          }          
        });
        this.UpdateValues();
        this.lastUpdate = new Date(); 
      });
      
    }
  }

  UpdateValues() {
    for (let currency of this.currencies) {
      if(this.data.labels.indexOf(currency.date) === -1)
      {
        this.data.labels.push(currency.date);
        this.data.series[0].push(currency.value);
      }
    }
  }

  getLabelsAndValues() {
    let labels: Array<Date> = [];
    let series: Array<Array<number>> = [[]];

    for (let currency of this.currencies) {
      labels.push(currency.date);
      series[0].push(currency.value);
    }
    this.data = {
      series: series,
      labels: labels
    }

  }


}
interface IChartData
{
  labels: Array<Date>;
  series: Array<Array<number>>;
}