import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable()
export class DemoApiService {

 endPoint: string = 'https://localhost:44308/api/';


  constructor(private http: HttpClient) { }

  getAvailableCurrencies():Observable<string[]>
  {
    return this.http.get<string[]>(this.endPoint + 'CurrencyCodes');
  }

  getStoredCurrencies():Observable<string[]>
  {
    return this.http.get<string[]>(this.endPoint + 'GetStoredCurrencies');
  }

  getCurrency(currencyName: string):Observable<any>
  {
    return this.http.get(this.endPoint + 'GetCurrency/' + currencyName);
  }

  getUpdatedCurrency(currencyName: string, updateDate:Date):Observable<any>
  {
    return this.http.get(this.endPoint + 'GetCurrency/' + currencyName + "/" + this.formatDate(updateDate));
  }
  
  getMonthCurrency(currencyName: string, monthEndDate:Date):Observable<any>
  {    
    return this.http.get(this.endPoint + 'GetMonthlyCurrency/' + currencyName + "/"+ this.formatDate(monthEndDate));
  }

  addCurrency(currencyName: string, date: Date):Observable<any>
  {
    return this.http.post(this.endPoint + 'AddCurrency/' + currencyName + "/?currencyDate=" + this.formatDate(date), null);
  }

  private formatDate(date: Date)
  {
    let dateString = JSON.stringify(date);
    return dateString.substring(1,dateString.length -1);
  }
}
