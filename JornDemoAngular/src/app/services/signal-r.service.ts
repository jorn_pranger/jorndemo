import { Injectable, EventEmitter } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
 
private hubConnection: signalR.HubConnection
 data:any;
 receivedUpdate: EventEmitter<string> = new EventEmitter<string>();

   public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
                            .withUrl('https://localhost:44308/CurrencyHub' )
                            .build();
 
    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err))
  }
 
  public addTransferChartDataListener = () => {
    this.hubConnection.on('UpdatedCurrency', (data) => {
    this.receivedUpdate.emit(data);
    });
  }

  public updateCurrency(currencyCode: string, currencyDate: Date): Promise<any>
  {
    return this.hubConnection.invoke<string>("AddCurrency", currencyCode,currencyDate);
  }
}