import { TestBed } from '@angular/core/testing';

import { DemoapiService } from './demoapi.service';

describe('DemoapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DemoapiService = TestBed.get(DemoapiService);
    expect(service).toBeTruthy();
  });
});
