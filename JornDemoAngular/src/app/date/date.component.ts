import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.sass']
})
export class DateComponent implements OnInit {
  @Output() selectDate: EventEmitter<Date> = new EventEmitter();
  currencyDate: Date;
  constructor() { }

  ngOnInit() {
  }
  dateChanged() {
    this.selectDate.emit(this.currencyDate);
  }
}
