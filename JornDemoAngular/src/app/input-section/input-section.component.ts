import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CurrencySelection } from '../models'
import { DemoApiService } from '../services/demoapi.service';
import { SignalRService } from '../services/signal-r.service';

@Component({
  selector: 'app-input-section',
  templateUrl: './input-section.component.html',
  styleUrls: ['./input-section.component.sass']
})
export class InputSectionComponent implements OnInit {

  @Output() onSelectValues: EventEmitter<CurrencySelection> = new EventEmitter();
  service: DemoApiService;
  signalRService: SignalRService;

  constructor(service: DemoApiService, signalRService: SignalRService) {
    this.service = service;
    this.signalRService = signalRService;
  }

  selectedValues: CurrencySelection;


  ngOnInit() {
    this.selectedValues = new CurrencySelection();
  }

  onButtonClick() {
    if (this.selectedValues.date && this.selectedValues.name) {

      this.signalRService.updateCurrency(this.selectedValues.name, this.selectedValues.date).finally(() => { this.onSelectValues.emit(this.selectedValues) });
    }
  }
  UpdateDate(message: Date) {
    this.selectedValues.date = message;
  }
  UpdateCurrency(message: string) {
    this.selectedValues.name = message;
  }

}
