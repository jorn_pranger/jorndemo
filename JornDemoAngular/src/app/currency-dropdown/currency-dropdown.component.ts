import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {DemoApiService} from '../services/demoapi.service'

@Component({
  selector: 'app-currency-dropdown',
  templateUrl: './currency-dropdown.component.html',
  styleUrls: ['./currency-dropdown.component.sass']
})
export class CurrencyDropdownComponent implements OnInit {
  @Output() selectCurrency: EventEmitter<string> = new EventEmitter();
  service: DemoApiService;
  currencies: string[]
  constructor(service: DemoApiService) 
  { 
    this.service = service;
  }

  ngOnInit() {
     this.service.getAvailableCurrencies().subscribe(data =>{
      this.currencies = data;
      this.selectCurrency.emit(data[0]);
    });  
  }
  currencyChanged(event: any) {
    this.selectCurrency.emit(event.target.value);
  }

}
