using JornDemo.BL;
using JornDemo.BL.CurrencyGetters;
using JornDemo.BL.CurrencyGetters.Helper;
using Moq;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Tests
{
    public class Tests
    {

        public Mock<IApiKeyReader> keyReader;
        public Mock<IApiResponseHelper> responseHelper;

        [SetUp]
        public void Setup()
        {
            keyReader = new Mock<IApiKeyReader>();
            keyReader.SetupGet(keyr => keyr.HasApiKey).Returns(true);
            keyReader.SetupGet(keyr => keyr.ApiKey).Returns("mock");
            responseHelper = new Mock<IApiResponseHelper>();
        }

        [Test]
        public void TestGetApiResponseReturnsCorrectValue()
        {
            responseHelper.Setup(resp => resp.GetApiResponse(It.IsAny<string>())).Returns(Task.FromResult(@"{""success"":true,""terms"":""https:\/\/currencylayer.com\/terms"",""privacy"":""https:\/\/currencylayer.com\/privacy"",""historical"":true,""date"":"""+ DateTime.Now.ToString("yyyy-MM-dd" ) + @""",""timestamp"":1483315199,""source"":""USD"",""quotes"":{""USDBTC"":0.001002}}"));
            ApiCurrencyGetter getter = new ApiCurrencyGetter(keyReader.Object, responseHelper.Object);
            Assert.AreEqual(1/0.001002 , getter.GetCurrencyValue("BTC" , DateTime.Now));
        }
    }
}